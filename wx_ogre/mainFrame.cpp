#include "mainFrame.h"

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//================================================================================= mainFrame()//
mainFrame::mainFrame(wxWindow* parent,wxWindowID id)
{
    //Attach menu items and the statusbar
    Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));

    //Create the main Ogre scene
    wxGridSizer* MainBoxSizer = new wxGridSizer(2,2,0,0);
    this->SetSizer(MainBoxSizer);
    MainBoxSizer->RecalcSizes();

    mpMainOgreView = new wxOgreView(this);
    mpSecOgreView = new wxOgreViewEventHandler(this);

    MainBoxSizer->Add(mpMainOgreView,0, wxSHAPED|wxALL|wxALIGN_CENTER,5);
    MainBoxSizer->Add(mpSecOgreView,0, wxSHAPED|wxALL|wxALIGN_CENTER,5);
    MainBoxSizer->SetSizeHints( this );

    this->Layout();
    mpMainScene = new wxOgreScene(mpMainOgreView, mpSecOgreView);
    SetSize(800,500);
}

// =============================================================================== ~mainFrame()
mainFrame::~mainFrame()
{
}
