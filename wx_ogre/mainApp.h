/** @mainpage
    This Application includes a main frame with a wxOgreview class that provides an
    Ogre RenderWindow and some useful methods. It can be applied to different wxWidgets/Ogre
    applications like editors or model viewers. There are also two wxOgreViewEventHandler
    classes that allow interaction with the render window using a mouse. These classes
    are used in the wxOgreScene class which creates a test scene with different viewports
    that is displayed in the render windows. The program is splitted into five classes
    to keep each class as simple and minimalistic as possible. The wxOgreScene class might
    look a little bit confusing, but you should not have problems understanding the major
    part of the source code.

    The application is free to use but you still have to observe the licenses of
    Ogre and wxWidgets.
    @version 1.0
    @author Evgenij Shyshkin & Thomas Sablik
    @note This application was created as a seminar task to be used in the editors
          of a robot simulation software which is developed as a Master's Thesis
          at the University of Wuppertal. For more information see the official site:
          http://dlbit.com/Seminar/
*/

/// The Application class
/** The mainApp class is used to create the main frame
    during the initialisation. You can modify this class to
    get the appropriate behaviour for your own application.
*/
#ifndef MAINAPP_H
#define MAINAPP_H

//(*AppHeaders
#include <wx/app.h>
#include "mainFrame.h"
#include <wx/image.h>
#include "wxOgreView.h"
//*)

class mainApp : public wxApp
{
    public:
        /** Create the main frame on initialising*/
        virtual bool OnInit();
};

#endif // MAINAPP_H
